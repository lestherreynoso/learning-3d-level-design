﻿using UnityEngine;
using System.Collections;

public class ThunderClap : MonoBehaviour {

    public AudioClip clip;
    public bool canFlicker = true;

    IEnumerator Flicker()
    {
        if (canFlicker)
        {
            canFlicker = false;
            GetComponent<AudioSource>().PlayOneShot(clip);
            GetComponent<Light>().enabled = true;
            yield return new WaitForSeconds(Random.Range(0.1f, 0.4f));
            GetComponent<Light>().enabled = false;
            yield return new WaitForSeconds(Random.Range(0.1f, 5f));
            canFlicker = true;

        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        StartCoroutine(Flicker());
	}
}
